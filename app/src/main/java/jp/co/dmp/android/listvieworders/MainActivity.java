package jp.co.dmp.android.listvieworders;           // a signs from android manifest

// after that, loading from here (like include in C)
import android.os.Bundle;
import android.app.Activity;
import android.widget.ListView;

import java.util.ArrayList;

import jp.co.dmp.android.listvieworders.adapters.OrderAdapter;
import jp.co.dmp.android.listvieworders.models.Orders;

// working from here
public class MainActivity extends Activity  {
    private ArrayList<Orders> arrayList = new ArrayList<Orders>();  // new?

// security levels
// adding parent class (super)
    @Override   // do not use parent class of onCreate
    protected void onCreate(Bundle savedInstanceState) {
        // call the parent class +
        super.onCreate(savedInstanceState);

        // set dummy data before set the list
        initData();

        // set main layout
        setContentView(R.layout.activity_main);
        // set list, adapter
        ListView listView = (ListView)findViewById(R.id.list_view);
        OrderAdapter adapter = new OrderAdapter(this, R.layout.adapter_item, arrayList);

        // put the list data on ui
        listView.setAdapter(adapter);

//        Log.d("MainActivity", "hi" );
//        listView.setOnItemClickListener(this);
    }

    private void initData() {
        for (int i = 0 ; i < 100; i++) {
            Orders order = new Orders();
            order.setCustomer("Customer " + i);
            order.setAddress("" + i);

            arrayList.add(order);
        }
    }
}

