package jp.co.dmp.android.listvieworders.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import jp.co.dmp.android.listvieworders.R;
import jp.co.dmp.android.listvieworders.models.Orders;

/**
 * Created by Jeongjoo on 2015. 12. 21..
 */
public class OrderAdapter extends ArrayAdapter<Orders> {

    // fields
    private Context context;
    private int resource;
    private List<Orders> objects = null;

    // class
    private static class ViewHolder {
        TextView customer;
        TextView address;
    }

    // constructor
    public OrderAdapter(Context context, int resource, List<Orders> objects) {
        super(context, resource, objects); // call the parent
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    @Override
    public int getCount() {
        super.getCount();
        int count = 0;

        if (objects != null) {
            count = objects.size();
        }

        return count;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            // create view
            LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(resource, null);  // assigning

            // textView holder
            ViewHolder holder = new ViewHolder();
            holder.customer = (TextView)view.findViewById(R.id.customer);
            holder.address = (TextView)view.findViewById(R.id.address);
            view.setTag(holder);    // saving
        }

        final Orders order = objects.get(position); // final?
        if (order != null) {
            final ViewHolder holder = (ViewHolder)view.getTag();    // loading, final?
            // set text on the textview
            holder.customer.setText(order.getCustomer());
            holder.address.setText(order.getAddress());
        }

        return view;
    }
}
