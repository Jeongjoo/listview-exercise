package jp.co.dmp.android.listvieworders.models;

/**
 * Created by Jeongjoo on 2015. 12. 21..
 */
public class Orders {
    private String customer;
    private String address;

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        // this meamns this class instance customer
        this.customer = customer;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        // this meamns this class instance address
        this.address = address;
    }
}
